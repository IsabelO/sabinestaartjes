﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotater : MonoBehaviour
{
    [SerializeField]
    float rotationSpeed;
    Quaternion startRotation;
    // Update is called once per frame

    void Update()
    {
        //transform.rotation = startRotation.x, transform.rotation.eulerAngles.y + rotationSpeed, startRotation.z);
        transform.Rotate(0, rotationSpeed, 0, Space.World);
        
    }
}
