﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandCorrector : MonoBehaviour
{
    [SerializeField]
    private GameObject tracker;
    // Start is called before the first frame update
    private enum Trackers { LH_Tracker, RH_tracker };
    [SerializeField]
    private Trackers ChooseHand;

    void Start()
    {
        Debug.Log("Hij doet 't hoor");
        if (tracker != null)
        {
            if (ChooseHand == Trackers.LH_Tracker)
            {
                Debug.Log("Hand correction applied");
                tracker.transform.rotation = Quaternion.Euler(0, -90, 0);
            }
            else if (ChooseHand == Trackers.RH_tracker)
            {
                Debug.Log("Hand correction applied");
                tracker.transform.rotation = Quaternion.Euler(0, 90, 0);
            }

        }
        else
        {
            Debug.LogWarning("Tracker hasn't been assigned in the HandCorrector script.");
        }
    }
    void OnEnable()
    {
        Start();
    }
}
