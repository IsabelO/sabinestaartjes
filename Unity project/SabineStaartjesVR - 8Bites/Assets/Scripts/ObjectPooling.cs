﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling<T> where T : class
{
    private Func<T> produce;
    private List<T> objects;
    private Func<T, bool> used;
    private bool expand;
    private int initialSize;

    public ObjectPooling(Func<T> instantiate, int maxSize, Func<T, bool> inUse, bool expand = false)
    {
        produce = instantiate;
        initialSize = maxSize;
        this.expand = expand;
        used = inUse;
        objects = new List<T>(maxSize);
    }

    public T GetInstance()
    {
        var count = objects.Count;
        foreach (var item in objects)
        {
            if (!used(item))
            {
                return item;
            }
        }
        if (count >= initialSize && !expand)
        {
            return null;
        }
        var obj = produce();
        objects.Add(obj);
        return obj;
    }
}