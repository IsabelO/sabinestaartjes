﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR; 

public class ShowMenu : MonoBehaviour
{

    // a reference to the action
    public SteamVR_Action_Boolean MenuOnOff;

    // a reference to the hand
    public SteamVR_Input_Sources handType;

    // a reference to the canvas 
    public GameObject Canvas; 

    // Start is called before the first frame update
    void Start()
    {
        MenuOnOff.AddOnStateDownListener(TriggerDown, handType);
       // MenuOnOff.AddOnStateUpListener(TriggerUp, handType);
    }


    public void TriggerDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        Debug.Log("Trigger is down");
        if (Canvas.activeSelf)
        {
            Canvas.SetActive(false);
            Debug.Log("Trigger uit");
        }
        else
        {
            Canvas.SetActive(true);
            Debug.Log("Trigger aan");
        }
    }

    void Update()
    {
        
    }

}
