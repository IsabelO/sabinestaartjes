﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : MonoBehaviour
{
    [SerializeField]
    private float lifetime;
    private float trailLife = 2f;
    [SerializeField]
    private float speed;
    private Vector3 originalScale;
    private float minimum= 0.12f;
    private float maximum = 0.17f;
    private float minimumY;
    private float maximumY;

    private float t;

    protected void OnEnable()
    {
        originalScale = transform.localScale;
        Debug.Log("hoi");
        GetComponent<Rigidbody>().AddForce(transform.forward * speed, ForceMode.VelocityChange);
        Invoke("ConfettiBlast", lifetime);
    }

    protected void Reset()
    {
        Debug.Log("reset");
        GetComponent<MeshRenderer>().enabled = true;
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        gameObject.SetActive(false);
    }

    void ConfettiBlast()
    {
        GetComponent<MeshRenderer>().enabled = false;
        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        Invoke("Reset", trailLife);
    }
    /*
    void Update()
    {
        transform.localScale = new Vector3(Mathf.Lerp(minimum, maximum, t), Mathf.Lerp(minimum, maximum, t), 0.15f);
        //transform.position = new Vector3(transform.position.x, Mathf.Lerp(minimumY, maximumY, t), transform.position.z);
 
        t += 0.5f * Time.deltaTime;

        if (t > 1.0f)
        {
            float temp = maximum;
            maximum = minimum;
            minimum = temp;
            t = 0.0f;
        }
    }*/
    void OnCollisionEnter(Collision collision)
    {
        CancelInvoke();
        ConfettiBlast();
    }

    protected void OnDisable()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        CancelInvoke();
    }


}
