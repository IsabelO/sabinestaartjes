﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Shooter : MonoBehaviour
{
    [Header("Object Pool")]
    public ObjectPooling<GameObject> bubbles;
    [SerializeField]
    private int projectilePoolSize = 10;
    [SerializeField]
    private bool expandable = true;
    [SerializeField]
    private GameObject shooterTip;
    [SerializeField]
    private GameObject bubble;

    // a reference to the action
    public SteamVR_Action_Boolean Triggered;
    // a reference to the hand
    public SteamVR_Input_Sources handType;

    void Awake()
    {
        Triggered.AddOnStateDownListener(TriggerDown, handType);
        //SphereOnOff.AddOnStateUpListener(TriggerUp, handType);
    }
    // Start is called before the first frame update
    void Start()
    {

        bubbles = new ObjectPooling<GameObject>(() => Instantiate(bubble, shooterTip.transform.position, shooterTip.transform.rotation), projectilePoolSize, p => p.activeInHierarchy, expandable);
    }

    public void TriggerDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        Debug.Log("Trigger is down");
        BlowBubble(bubbles.GetInstance());
        Debug.Log("BUBBLES ARE BEING BLOWN");

    }

    void BlowBubble(GameObject projectile)
    {
        if (projectile != null)
        {
            var body = projectile.GetComponent<Rigidbody>();
            body.transform.position = shooterTip.transform.position;
            body.transform.rotation = shooterTip.transform.rotation;
            projectile.SetActive(true);
        }
    }
}
