﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SwitchColours : MonoBehaviour
{

    public List<GameObject> floorTiles;
    public Color[] tilesColors;
   
    public float startTime = 0.5f;
    private float time;
    // public float speed = 1.0f; 
    // public Color startColor; 
    // public Color endColor; 
    // public bool repeatable = false; 
    // float startTime; 

    // Use this for initialization 
    void Start ()
    {
        //startTime = Time.time; 
        time = startTime;
        for (int i = 0; i < GameObject.FindGameObjectsWithTag("tile").Length; i++)
        {
            floorTiles.Add(GameObject.FindGameObjectsWithTag("tile")[i]);
        }

    }

    // Update is called once per frame 
    void Update ()
    {
        time -= Time.deltaTime;
        if (time <= 0)
        {
            ChangeColor();
            time = startTime;
        }


        // if (!repeatable) 
        // { 
        //     float t = (Time.time - startTime) * speed; 
        //     GetComponent<Renderer>().material.color = Color.Lerp(startColor, endColor, t); 
        // } 
        // else 
        // { 
        //     float t = (Mathf.Sin(Time.time - startTime) * speed); 
        //     GetComponent<Renderer>().material.color = Color.Lerp(startColor, endColor, t); 
        // } 


    }

    void ChangeColor ()
    {
        for (int i = 0; i < floorTiles.ToArray().Length; i++)
        {
            //execute 

            //random getal berekenen 
            int randomIndexNumber;// = Random.Range(0, (tilesColors.Length - 1)); 
            // kleur gelijk aan de value uit de colorList met de index(randomgetal) 
            Color color;// = tilesColors[randomIndexNumber]; 

            do
            {
                randomIndexNumber = Random.Range(0, (tilesColors.Length - 1));
                // kleur gelijk aan de value uit de colorList met de index(randomgetal) 
                color = tilesColors[randomIndexNumber];
            }
            while (color == floorTiles[i].GetComponent<Renderer>().material.color);

            floorTiles[i].GetComponent<Renderer>().material.SetColor("_BaseColor", color);

        }

    }
}